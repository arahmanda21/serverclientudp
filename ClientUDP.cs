﻿/*
C# Network Programming 
by Richard Blum

Publisher: Sybex 
ISBN: 0782141765
*/
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpClient
{
   public static void Main()
   {
      byte[] data = new byte[1024];
      string input = " ", stringData;
      IPEndPoint ipep = new IPEndPoint(
                      IPAddress.Parse("10.252.39.162"), 9050);

      Socket server = new Socket(AddressFamily.InterNetwork,
                     SocketType.Dgram, ProtocolType.Udp);

      Console.Write("Masukkan username : ");
      string username = Console.ReadLine();
      data = Encoding.ASCII.GetBytes(username);
      server.SendTo(data, data.Length, SocketFlags.None, ipep);

      IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
      EndPoint Remote = (EndPoint)sender;

      data = new byte[1024];
      int recv = server.ReceiveFrom(data, ref Remote);

      Console.WriteLine("Menerima pesan dari {0}:", Remote.ToString());
      string serverUsername = Encoding.ASCII.GetString(data, 0, recv);
      Console.WriteLine(serverUsername + "/> Hai, Namaku " + serverUsername + ". Aku servermu!");
	  
      while (true) {
      	if (input == " ") {
      		Console.Write(username + "/> ");
      	   	input = Console.ReadLine();
      	   	if (input == "exit")
      	 		break;
      	   	server.SendTo(Encoding.ASCII.GetBytes(input), Remote);
      	}
      	
      		data = new byte[1024];
        	recv = server.ReceiveFrom(data, ref Remote);
        	stringData = Encoding.ASCII.GetString(data, 0, recv);
        		if (stringData == "exit")
        			break;
        	Console.WriteLine(serverUsername + "/> " + stringData);
        	
        	Console.Write(username + "/> ");
      	   	input = Console.ReadLine();
      	   	server.SendTo(Encoding.ASCII.GetBytes(input), Remote);
      	   	
      	   	if (input == "exit")
      	 			break;
      }
      
      Console.WriteLine("Klien Berhenti");
      Console.ReadLine();
      server.Close();
   }
}