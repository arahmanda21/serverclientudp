﻿/*
C# Network Programming 
by Richard Blum

Publisher: Sybex 
ISBN: 0782141765
*/
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UdpSrvrSample
{
   public static void Main()
   {
      byte[] data = new byte[1024];
      string input = " ";
      IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050);
      UdpClient newsock = new UdpClient(ipep);

      Console.Write("Masukkan username : ");
      string username = Console.ReadLine();
      Console.WriteLine("Menunggu klien...");

      IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

      data = newsock.Receive(ref sender);
      string clientUsername = Encoding.ASCII.GetString(data, 0, data.Length);

      Console.WriteLine("Menerima pesan dari {0}:", sender.ToString());
      Console.WriteLine(clientUsername + "/> Hai, namaku " + Encoding.ASCII.GetString(data, 0, data.Length));

      data = Encoding.ASCII.GetBytes(username);
      newsock.Send(data, data.Length, sender);

      while(true)
      {
         data = newsock.Receive(ref sender);
         if (Encoding.ASCII.GetString(data, 0, data.Length) == "exit")
         	break;
         Console.WriteLine(clientUsername + "/> " + Encoding.ASCII.GetString(data, 0, data.Length));
         
         Console.Write(username + "/> ");
      	 input = Console.ReadLine();
      	 data = Encoding.ASCII.GetBytes(input);
      	 newsock.Send(data, data.Length, sender);
      	 
      	 if (input == "exit")
      	 	break;
      }
      
      Console.WriteLine("Server Berhenti");
      Console.ReadLine();
   }
}